'use strict';

var katana = angular.module('katana');
katana.controller('SpeakerController', function($scope, $ionicActionSheet, Speakers) {
  $scope.speakers = [];
  Speakers.get(function(result) {
    $scope.speakers = result.data;
  });

  // Triggered on a button click, or some other target
  $scope.shareSpeaker = function() {
    // Show the action sheet
    var hideSheet = $ionicActionSheet.show({
      buttons: [{
        text: '<b>Share</b> This'
      }, {
        text: 'Move'
      }],
      destructiveText: 'Delete',
      titleText: 'Modify your album',
      cancelText: 'Cancel',
      cancel: function() {
        // add cancel code..
      },
      buttonClicked: function(index) {
        return true;
      }
    });

  };
});