'use strict';

var katana = angular.module('katana');
katana.controller('AccountController', function($scope, $localStorage, $state, Account) {
    $scope.account = {};

    $scope.settingsList = [{
        text: "Enable news from iBorrow",
        checked: true
    }];
    $scope.signIn = function() {
        Account.save($scope.account, function(result) {
            $localStorage.userToken = result;
            $state.go('katana.conference.schedule');
        });
    };

    $scope.signUp = function() {
        Account.save($scope.account, function(result) {
            $localStorage.account = result.data;
        });
    };
});