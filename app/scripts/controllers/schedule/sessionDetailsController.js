'use strict';

var katana = angular.module('katana');
katana.controller('SessionDetailsController', function($scope, $stateParams,$ionicModal) {
    $scope.id = $stateParams.id;
    $scope.session = {
        "id": 1,
        "name": "Introduction to Appcamp.io",
        "location": "Room 2203",
        "description": "Mobile devices and browsers are now advanced enough that developers can build native-quality mobile apps using open web technologies like HTML5, Javascript, and CSS. In this talk, we’ll provide background on why and how we created Ionic, the design decisions made as we integrated Ionic with Angular, and the performance considerations for mobile platforms that our team had to overcome. We’ll also review new and upcoming Ionic features, and talk about the hidden powers and benefits of combining mobile app development and Angular.",
        "speakerNames": ["Brandy Carney"],
        "timeStart": "9:00 am",
        "timeEnd": "9:30 am",
        "tracks": ["Ionic"]
    };

 $ionicModal.fromTemplateUrl('views/katana/questionDialog.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modal = modal;
  });
  $scope.openModal = function() {
    $scope.modal.show();
  };
  $scope.closeModal = function() {
    $scope.modal.hide();
  };
  //Cleanup the modal when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.modal.remove();
  });
  // Execute action on hide modal
  $scope.$on('modal.hidden', function() {
    // Execute action
  });
  // Execute action on remove modal
  $scope.$on('modal.removed', function() {
    // Execute action
  });
});
