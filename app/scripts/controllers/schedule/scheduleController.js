'use strict';

var katana = angular.module('katana');
katana.controller('ScheduleController', function($scope, Schedule) {
	$scope.schedules = [];
    Schedule.get(function(schedules) {
        $scope.schedules = schedules.groups;
    });
});