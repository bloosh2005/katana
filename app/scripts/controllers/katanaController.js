'use strict';

var katana = angular.module('katana');

katana.controller('KatanaController', function($scope, $ionicPopover, $translate, $state, AccountService, Language) {
  //when skip the information page,start the katana application
  $scope.startKatana = function() {
    $state.go('tachi.account.signUp');
  };

  Language.query(function(result) {
    $scope.languages = result;
  }, function(error) {

  });

  $scope.user = AccountService.getAccount();

  $scope.changeLanguage = function(key) {
    $translate.use(key);
  };
  $scope.account = {
    language: 'zh_CN'
  };
  $scope.getLanguageText = function(value) {
    return _.result(_.find($scope.languages, function(theOne) {
      return theOne.value === value;
    }), 'text');
  };

  //for seetings popover use
  $ionicPopover.fromTemplateUrl('views/katana/settingsPopover.html', {
    scope: $scope
  }).then(function(popover) {
    $scope.popover = popover;
  });

  $scope.openPopover = function($event) {
    $scope.popover.show($event);
  };
  $scope.closePopover = function() {
    $scope.popover.hide();
  };
  //Cleanup the popover when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.popover.remove();
  });
  // Execute action on hide popover
  $scope.$on('popover.hidden', function() {
    // Execute action
  });
  // Execute action on remove popover
  $scope.$on('popover.removed', function() {
    // Execute action
  });

});