'use strict';

var katana = angular.module('katana');

katana
    .factory("Language", function($resource) {
        return $resource('api/languages/languages.json');
    })
    .factory("TheAccount", function($resource) {
        return $resource('http://127.0.0.1:3000/account/attendConference');
    })
    .factory("Question", function($resource) {
        return $resource('http://127.0.0.1:3000/account/askQuestion');
    })
    .factory("Account", function($resource) {
        return $resource('http://127.0.0.1:3000/accounts/authenticate');
    })
    .factory("Schedule", function($resource) {
        return $resource('api/schedule.json');
    })
    .factory("Speakers", function($resource) {
        return $resource('http://127.0.0.1:3000/speakers');
    });
