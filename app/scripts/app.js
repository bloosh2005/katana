'use strict';

/**
 * @ngdoc overview
 * @name katana
 * @description
 * # katana
 *
 * Main module of the application.
 */
var katana = angular.module('katana', [
  'ionic',
  'ngResource',
  'ui.router',
  'ngStorage',
  'pascalprecht.translate',
  'angular-jwt'
]);
katana.config(function($stateProvider, $urlRouterProvider, $resourceProvider, $translateProvider, $ionicConfigProvider, $httpProvider, jwtInterceptorProvider) {

  $httpProvider.interceptors.push(function($rootScope, $injector) {
    return {
      request: function(config) {
        $rootScope.$broadcast('loading:show');
        return config;
      },
      response: function(response) {
        $rootScope.$broadcast('loading:hide');
        return response;
      },
      responseError: function(rejection) {
        $rootScope.$broadcast('loading:hide');
        console.log(JSON.stringify(rejection));
        if (rejection.data === null) {

        } else if (rejection.status === 401) {
          $injector.invoke(function($state) {
            console.log($state.current.name);
            $state.go('insight.signIn');
          });
        } else if (rejection.status === 403) {

        } else if (rejection.status === 404) {

        } else if (rejection.status === 500) {

        }
      }
    };
  });
  //Enable cross domain calls
  $httpProvider.defaults.useXDomain = true;
  jwtInterceptorProvider.authPrefix = 'Bearer ';
  jwtInterceptorProvider.tokenGetter = ['config', 'jwtHelper', '$localStorage', function(config, jwtHelper, $localStorage) {
    var theUserToken = $localStorage.userToken;
    if (theUserToken instanceof Object) {
      var date = jwtHelper.getTokenExpirationDate(theUserToken.token);
      var bool = jwtHelper.isTokenExpired(theUserToken.token);
      return theUserToken.token;
    } else {
      //do nothing with rejected with 401 Unauthorized error
    }
  }];
  $httpProvider.interceptors.push('jwtInterceptor');
  $translateProvider.useStaticFilesLoader({
    prefix: 'api/languages/',
    suffix: '.json'
  });
  $translateProvider.preferredLanguage('zh_CN');
  $translateProvider.fallbackLanguage('zh_EN');
  $ionicConfigProvider.tabs.position("bottom"); //Places them at the bottom for all OS
  $ionicConfigProvider.tabs.style("standard"); //Makes them all look the same across all OS



  $stateProvider

  .state('tachi', {
      url: "/tachi",
      abstract: true,
      templateUrl: "views/tachi/tachi.html",
      controller: 'KatanaController'
    })

    .state('tachi.introduction', {
      url: "/introduction",
      views: {
        'tachiContent': {
          templateUrl: "views/katana/introduction.html",
          controller: 'KatanaController'
        }
      }
    })
    .state('tachi.account', {
      url: "/account",
      abstract: true,
      views: {
        'tachiContent': {
          templateUrl: "views/account/accountTabs.html",
          controller: 'AccountController'
        }
      }
    })
    .state('tachi.account.signIn', {
      url: "/signIn",
      views: {
        'signInContent': {
          templateUrl: "views/account/signIn.html",
          controller: 'AccountController'
        }
      }
    })
    .state('tachi.account.signUp', {
      url: "/signUp",
      views: {
        'signUpContent': {
          templateUrl: "views/account/signUp.html",
          controller: 'AccountController'
        }
      }
    })
    .state('katana', {
      url: "/katana",
      abstract: true,
      templateUrl: "views/menu.html",
      controller: 'KatanaController'
    })

  .state('katana.conference', {
      url: "/conference",
      abstract: true,
      views: {
        'menuContent': {
          templateUrl: "views/conference/conferenceTabs.html",
          controller: 'ConferenceController'
        }
      }
    })
    .state('katana.conference.schedule', {
      url: "/schedule",
      views: {
        'scheduleContent': {
          templateUrl: "views/conference/schedule/schedule.html",
          controller: 'ScheduleController'
        }
      }
    })
    .state('katana.conference.speakers', {
      url: "/speakers",
      views: {
        'speakersContent': {
          templateUrl: "views/conference/speakers/speakers.html",
          controller: 'SpeakerController'
        }
      }
    })
    .state('katana.conference.about', {
      url: "/about",
      views: {
        'aboutContent': {
          templateUrl: "views/conference/about.html",
          controller: 'ConferenceController'
        }
      }
    })
    .state('katana.conference.session', {
      url: "/session/:id",
      views: {
        'scheduleContent': {
          templateUrl: "views/conference/schedule/sessionDetails.html",
          controller: 'SessionDetailsController'
        }
      }
    })
    .state('katana.conference.speaker', {
      url: "/speaker/:id",
      views: {
        'speakersContent': {
          templateUrl: "views/conference/speakers/speakerDetails.html",
          controller: 'SpeakerController'
        }
      }
    })
    .state('katana.exams', {
      url: "/exams",
      views: {
        'menuContent': {
          templateUrl: "views/exam/exams.html",
          controller: 'ExamController'
        }
      }
    })
    .state('katana.exam', {
      url: "/exam/:id",
      views: {
        'menuContent': {
          templateUrl: "views/exam/examDetails.html",
          controller: 'ExamDetailsController'
        }
      }
    })
    .state('katana.account', {
      url: "/account",
      views: {
        'menuContent': {
          templateUrl: "views/account/account.html",
          controller: 'AccountController'
        }
      }
    })
    // notice that i just put the account setting here
    .state('katana.settings', {
      url: "/settings",
      views: {
        'menuContent': {
          templateUrl: "views/account/settings.html",
          controller: 'AccountController'
        }
      }
    });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/tachi/introduction');
});

katana.run(function($ionicPlatform, $rootScope, $ionicLoading) {
  $ionicPlatform.ready(function() {
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
  //register listeners for loading
  $rootScope.$on('loading:show', function() {
    $ionicLoading.show();
  });
  $rootScope.$on('loading:hide', function() {
    $ionicLoading.hide();
  });
});
